export class Post{
    id: number | undefined;
    title!: string;
    content!: string;
    orgNo!: string;
    createdDate!: string;
    updatedDate!: string;
    status: boolean = true;
}