import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './guards/auth.guard';
import { LoginformComponent } from './components/loginform/loginform.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { PostListComponent } from './components/post/post-list/post-list.component';
import { PostDetailComponent } from './components/post/post-detail/post-detail.component';
import { UserformComponent } from './components/userform/userform.component';

const routes: Routes = [
  {path: '', component:DashboardComponent, pathMatch:'full', canActivate: [AuthGuard]},
  {path: 'login', component:LoginformComponent},
  {path: 'dashboard', component:DashboardComponent, canActivate: [AuthGuard]},
  {path: 'postlist', component:PostListComponent, canActivate: [AuthGuard]},
  { path: 'post-add/:id', component: PostDetailComponent, canActivate: [AuthGuard]},
  { path: 'post-edit/:id', component: PostDetailComponent, canActivate: [AuthGuard]},
  {path: 'userform', component:UserformComponent, canActivate: [AuthGuard]},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
