import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Subscription } from 'rxjs';
import { Subject, takeUntil } from 'rxjs';
import { Router,ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatSelectModule } from '@angular/material/select';
import { FormsModule,ReactiveFormsModule,FormControl }   from '@angular/forms';
import { ErrorMessageComponent } from '../../error-message/error-message.component';
import { MatInputModule } from '@angular/material/input';
import { MatCardModule } from '@angular/material/card';
import { PostService } from '../../../services/post.service'
import { Post } from 'src/app/models/model-data';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import {MatNativeDateModule} from '@angular/material/core';

@Component({
  selector: 'app-post-detail',
  standalone: true,
  imports: [CommonModule,MatFormFieldModule,MatInputModule,MatCardModule,MatCheckboxModule,MatDatepickerModule,MatSelectModule,
    FormsModule,ReactiveFormsModule,ErrorMessageComponent,MatGridListModule,MatButtonModule,MatIconModule,MatNativeDateModule],
  templateUrl: './post-detail.component.html',
  styleUrls: ['./post-detail.component.scss']
})
export class PostDetailComponent {
  private subscription: Subscription | undefined;
  postForm!: FormGroup;
  post: Post = new Post();
  createdDate = new FormControl(new Date());
  updatedDate = new FormControl(new Date());
  isLoading: boolean = false;
  private destroy$ : Subject<void> = new Subject<void>();

  constructor(private postService:PostService,private formBuilder: FormBuilder,
    private router: Router, private activatedRouter: ActivatedRoute, private snackBar: MatSnackBar){

      this.postForm = formBuilder.group({
        title: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(20)]],
        content: ['', Validators.required],
        orgNo: ['', Validators.required],
      });
  }

  ngOnInit(){
    const id = parseInt(this.activatedRouter.snapshot.paramMap.get("id") || '');
    if(id !== 0){
      this.postService.getPostById(id)
      .pipe(takeUntil(this.destroy$))
      .subscribe(data => {
        this.post = data;
        this.createdDate.setValue(new Date(this.post.createdDate));
        //this.editCreditCardForm.patchValue(this.creditCardData);
      });
    }
  }

  onSave() {
    if(!this.isLoading){
      this.isLoading = true;
       if(this.postForm.valid){
        console.log(this.post);
        this.postService.savePost(this.post)
        .pipe(takeUntil(this.destroy$))
        .subscribe({
          next: (data: any) => {
            this.goBack();
          },
          error: (err: Error) => {
            console.log("not success");
            console.log(err);
            this.showSuccessMessage(err.message);
          },
          complete: () => this.isLoading = false
        });
       }
    }

  }

  showSuccessMessage(message: string){
    this.snackBar.open(message, 'Close', {
      duration: 10000
    })
  }

  ngOnDestory(){
    this.destroy$.next();
    this.destroy$.complete();
  }

  goBack(){
    this.router.navigate(['/postlist']);
  }
}
