import { Component } from '@angular/core';
import { Post } from '../../../../app/models/model-data';
import { SelectionModel } from '@angular/cdk/collections';
import { MatTableDataSource } from '@angular/material/table';
import { PostService } from '../../../../app/services/post.service';
import { PageEvent } from '@angular/material/paginator';
import { MatCheckboxChange } from '@angular/material/checkbox';
import { Router } from '@angular/router';

@Component({
  selector: 'app-post-list',
  templateUrl: './post-list.component.html',
  styleUrls: ['./post-list.component.scss']
})
export class PostListComponent {

  postList: Post[] = [];

  displayColumns = ["select", "id", "title", "content", "orgNo", "createdDate", "updatedDate", "actions"];

  pageSizeOptions: number[] = [5, 10, 15, 20];

  selection = new SelectionModel<Post>(true, []);
  postDataSource = new MatTableDataSource(this.postList);

  page: number = 0;
  pageSize: number = 10;
  filterObj: any = {
   'title': ''
  };
  totalElements: number = 0;
  // Track selected rows
 selectedRows = new Set<any>();

  constructor(private postService: PostService, private router: Router) {

  }

  ngOnInit(): void {
   this.fetchPost();
 }

 fetchPost(){
    this.postService.getPosts(this.filterObj, this.page, this.pageSize).subscribe({
     next: (data: any) => {
     this.postList = data.dataList;
     this.postDataSource = new MatTableDataSource(this.postList);
     this.totalElements = data.totalElements;
     console.log("success");
     return true;
     },
     error: (err: Error) => {
       console.log("not success");
       console.log(err);
     },
   });
 }

 changePage(event: PageEvent) {
   this.page = event.pageIndex;
   this.pageSize = event.pageSize;
   this.fetchPost();
}

// Perform actions on selected rows (e.g., delete)
onDeleteSelectedRows() {
 // Implement your logic here
 console.log('Selected rows:', this.selection.selected);
}

addNew(){
 this.router.navigate(['post-add/0']);
}

// Handle checkbox selection
onCheckboxChange(event: MatCheckboxChange, row: any) {
 // if (event.checked) {
 //   this.selection.select
 // } else {
 //   this.selectedRows.delete(row);
 // }
 if( event.checked)
 this.selection.isSelected(row) ? null : this.selection.select(row);
 else this.selection.isSelected(row) ? this.selection.deselect(row) : null;
}

isAllSelected(){
 const numSelected = this.selection.selected.length;
 const numRows = this.postList?.length;
 return numSelected === numRows;
}

masterToggle(event: MatCheckboxChange) {
  this.isAllSelected() ? 
      this.selection.clear() : 
         this.postDataSource.data.forEach(row => this.selection.select(row));
}
}
